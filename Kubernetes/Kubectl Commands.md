#Kubectl frequently used commands

kubectl get services --all-namespaces

kubectl get pods --all-namespaces

kubectl get nodes

kubectl get namespaces

kubectl get pvc #persistant volume claim

#Kubectl Exposing Service to MetalLB 

#Use after inital deployment of app

kubectl expose deployment rancher -n cattle-system --type=LoadBalancer --name=rancher-lb --port=443

kubectl get service/rancher-lb -n cattle-system
